from django.db import models
from recipes.models import Recipe

# Create your models here.


class RecipeStep(models.Model):
    step_number = models.SmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,
        related_name='steps',
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ['step_number']


class Ingredient(models.Model):
    amount = models.TextField(max_length=100)
    food_item = models.TextField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name='ingredients',
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ['food_item']
