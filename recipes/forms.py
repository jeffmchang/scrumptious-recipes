from django import forms
from recipes.models import Recipe
from django.forms import ModelForm

# Create _model_Form class that inherits
# from ModelForm


class RecipeForm(ModelForm):
    email_address = forms.EmailField(max_length=300)
    # Meta is an inner class that is used
    # to customize the form

    class Meta:
        model = Recipe
        fields = (
            "title",
            "picture",
            "description",
        )
