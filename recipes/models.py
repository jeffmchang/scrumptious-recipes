from django.db import models
from django.conf import settings

# Create your models here.
# create a class that inherits form models.Model


class Recipe(models.Model):
    # add properties to our model
    title = models.CharField(max_length=200)
    picture = models.URLField(blank=True)
    description = models.TextField()
    created_on = models.DateField(auto_now_add=True)
    is_published = models.BooleanField(default=False)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='recipes',
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.title
