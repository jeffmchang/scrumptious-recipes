from django.contrib import admin
# import the model that you want to register
from recipes.models import Recipe


@admin.register(Recipe)
# Register the model using a class that inherits from
# admin.ModelAdmin
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
        "created_on"
    )


# @admin.register(RecipeStep)
# class RecipeAdmin(admin.ModelAdmin):
#     list_display = (
#         'recipe_title',
#         'order',
#         'id'
#     )
