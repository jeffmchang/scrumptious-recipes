from django.shortcuts import redirect, render, get_object_or_404
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required


# Create an instance of Recipeform
# Passing in nothing because we are creating something


@login_required
def create_recipe(request):
    # if you click "create" and the form is filled
    # properly, it will save into the database
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("recipe_list")
    else:
        # Create a blank instance of RecipeForm, if this
        # is a GET request (you click this link)
        # it will take you to a form
        form = RecipeForm()
    # Create context and add form to it
    context = {
        "form": form
    }
    # Render recipe form view, render request,
    # pull template, and fill template with context
    return render(request, "recipes/create.html", context)


def redirect_to_recipe_list(request):
    return redirect('recipe_list')


def edit_recipe(request, id_value):
    if request.method == "POST":
        recipe = Recipe.objects.get(id=id_value)
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()

    context = {
        "form": form
    }
    return render(request, "recipes/edit.html", context)


def show_recipes(request, id_value):
    print(f"Recipe ID: {id_value}")

    # recipe = Recipe.objects.get(id=id)
    # get_object_or_404(Recipe, id=id)
    recipe = get_object_or_404(Recipe, id=id_value)
    print("Recipe Object: ", recipe)
    print("Recipe Title: ", recipe.title)
    print("Recipe Description: ", recipe.description)
    context = {
        "recipe_object": recipe
    }
    return render(request, "recipes/details.html", context)


def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes
    }
    return render(request, "recipes/list.html", context)


def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        'recipe_list': recipes,
    }
    return render(request, 'recipes/list.html', context)
